//
//  TaxiDetailViewController.swift
//  taxis
//
//  Created by Fabio Dela Antonio on 10/8/15.
//  Copyright © 2015 fabio. All rights reserved.
//

import UIKit
import MapKit

@objc class TaxiDetailViewController: UIViewController, MKMapViewDelegate {

    var taxi : BNTaxi!
    var geocoder = CLGeocoder()
    
    @IBOutlet weak var mapView : MKMapView!
    @IBOutlet weak var addressLabel : UILabel!
    
    static func taxiDetailViewController(taxi: BNTaxi) -> TaxiDetailViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let viewController = storyboard.instantiateViewControllerWithIdentifier("TaxiDetailViewController") as! TaxiDetailViewController
        
        viewController.taxi = taxi
        
        return viewController
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationItem.title = "Taxi \(taxi.driverId)"
        
        // Parallax
        let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: UIInterpolatingMotionEffectType.TiltAlongVerticalAxis)
        
        vertical.minimumRelativeValue = NSNumber(float: 40)
        vertical.maximumRelativeValue = NSNumber(float: -40)
        
        let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: UIInterpolatingMotionEffectType.TiltAlongHorizontalAxis)
        
        horizontal.minimumRelativeValue = NSNumber(float: 40)
        horizontal.maximumRelativeValue = NSNumber(float: -40)
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontal, vertical]
        
        mapView.addMotionEffect(group)
        mapView.delegate = self
    }
    
    override func viewDidAppear(animated: Bool) {
        
        super.viewDidAppear(animated)
        
        addressLabel.attributedText = NSAttributedString(BBCode: "[font name=\"OpenSans\" size=\"16\"]Carregando endereço...[/font]")
        
        mapView.setCenterCoordinate(taxi.coordinate, zoomLevel: 17, animated: false)
        mapView.removeAnnotations(mapView.annotations)
        mapView.addAnnotation(taxi)
        
        geocoder.reverseGeocodeLocation(CLLocation(latitude: taxi.coordinate.latitude, longitude: taxi.coordinate.longitude), completionHandler: {
            (placemarks: [CLPlacemark]?, error: NSError?) -> Void in
            
            if error != nil || placemarks == nil {
                
                self.addressLabel.attributedText = NSAttributedString(BBCode: "[font name=\"OpenSans\" size=\"16\"]Falha ao carregar endereço![/font]")
            }
            
            else {
                
                let placemark : CLPlacemark = placemarks![(placemarks?.endIndex)! - 1]
                let addressDictionary = placemark.addressDictionary!
                
                let address = NSMutableString(string: "[font name=\"OpenSans\" size=\"16\"]")
                
                if let street = addressDictionary["Street"] {
                    
                    address.appendString("\(street)\n")
                }
                
                if let city = addressDictionary["City"] {
                
                    address.appendString("\(city) ")
                }
                
                if let state = addressDictionary["State"] {
                    
                    address.appendString("\(state)")
                }
                
                address.appendString("[/font]")
                
                self.addressLabel.attributedText = NSAttributedString(BBCode: address as String)
            }
        })
    }
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
        let identifier = "ident"
        var pin = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier)
        
        if pin == nil {
            
            pin = BNTaxiAnnotation.init(annotation: annotation, reuseIdentifier: identifier)
        }
        
        pin?.annotation = annotation
        return pin
    }
}
