//
//  BNJSONRequest.h
//  taxis
//
//  Created by Fabio Dela Antonio on 10/5/15.
//  Copyright © 2015 fabio. All rights reserved.
//

#import "BNNetworkDownloadRequest.h"

@interface BNJSONRequest : BNNetworkDownloadRequest

- (instancetype)initWithEndpoint:(NSString *)endpoint parameters:(NSDictionary *)params;

- (NSDictionary *)receivedHeader;

/* Override */
- (void)successWithResult:(id)result;
- (void)fail;

@end
