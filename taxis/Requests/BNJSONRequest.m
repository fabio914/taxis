//
//  BNJSONRequest.m
//  taxis
//
//  Created by Fabio Dela Antonio on 10/5/15.
//  Copyright © 2015 fabio. All rights reserved.
//

#import "BNJSONRequest.h"
#import <GoogleOpenSource/GoogleOpenSource.h>

#define TAXI_URL @"https://api.99taxis.com/"

@interface BNJSONRequest ()
@property (nonatomic, retain) NSDictionary * receivedHeader;
@end

@implementation BNJSONRequest

- (instancetype)initWithEndpoint:(NSString *)endpoint parameters:(NSDictionary *)params {
    
    NSString * urlString = [TAXI_URL stringByAppendingString:endpoint];
    
    if(params) {
        
        urlString = [urlString stringByAppendingFormat:@"?%@", [params gtm_httpArgumentsString]];
    }
    
    NSURL * url = [NSURL URLWithString:urlString];
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    if(self = [super initWithURLRequest:urlRequest]) {
        
    }
    
    return self;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    [_receivedHeader release], _receivedHeader = nil;
    _receivedHeader = [[(NSHTTPURLResponse *)response allHeaderFields] retain];
    [super connection:connection didReceiveResponse:response];
}

- (void)didDownloadData:(NSData *)data {
    
    if(data == nil) {
        
        [self fail];
        return;
    }
    
    NSError * error = nil;
    id jsonData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    
    if(error != nil) {
        
        [self fail];
        return;
    }
    
    [self successWithResult:jsonData];
}

- (void)successWithResult:(id)result {
    
}

- (void)fail {
    
}

- (void)dealloc {
    
    [_receivedHeader release], _receivedHeader = nil;
    [super dealloc];
}

@end