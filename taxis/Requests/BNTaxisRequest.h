//
//  BNTaxisRequest.h
//  taxis
//
//  Created by Fabio Dela Antonio on 10/5/15.
//  Copyright © 2015 fabio. All rights reserved.
//

#import "BNJSONRequest.h"
#import "BNTaxi.h"

@class BNTaxisRequest;

@protocol BNTaxisRequestDelegate <NSObject>

@required
- (void)taxisRequest:(BNTaxisRequest *)request didReturnTaxis:(NSArray *)taxis; // of BNTaxis
- (void)taxisRequestDidFail:(BNTaxisRequest *)request;

@end

@interface BNTaxisRequest : BNJSONRequest

@property (nonatomic, assign) id<BNTaxisRequestDelegate> delegate;

@property (nonatomic, assign, readonly) CLLocationCoordinate2D southWest;
@property (nonatomic, assign, readonly) CLLocationCoordinate2D northEast;

+ (instancetype)taxisRequestWithSouthWest:(CLLocationCoordinate2D)southWest northEast:(CLLocationCoordinate2D)northEast delegate:(id<BNTaxisRequestDelegate>)delegate;

@end
