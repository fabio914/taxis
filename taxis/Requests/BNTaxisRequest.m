//
//  BNTaxisRequest.m
//  taxis
//
//  Created by Fabio Dela Antonio on 10/5/15.
//  Copyright © 2015 fabio. All rights reserved.
//

#import "BNTaxisRequest.h"

@implementation BNTaxisRequest

+ (instancetype)taxisRequestWithSouthWest:(CLLocationCoordinate2D)southWest northEast:(CLLocationCoordinate2D)northEast delegate:(id<BNTaxisRequestDelegate>)delegate {
    
    return [[[self alloc] initWithSouthWest:southWest northEast:northEast delegate:delegate] autorelease];
}

- (instancetype)initWithSouthWest:(CLLocationCoordinate2D)southWest northEast:(CLLocationCoordinate2D)northEast delegate:(id<BNTaxisRequestDelegate>)delegate {
    
    NSString * sw = [NSString stringWithFormat:@"%lf,%lf", southWest.latitude, southWest.longitude];
    NSString * ne = [NSString stringWithFormat:@"%lf,%lf", northEast.latitude, northEast.longitude];
    
    if(self = [super initWithEndpoint:@"lastLocations" parameters:@{@"sw":sw, @"ne":ne}]) {
        
        _southWest = southWest;
        _northEast = northEast;
        _delegate = delegate;
    }
    
    return self;
}

- (void)successWithResult:(id)result {
    
    if(![result isKindOfClass:[NSArray class]]) {
        
        [self fail];
        return;
    }
    
    NSMutableArray * array = [NSMutableArray array];
    
    for(id entry in (NSArray *)result) {
        
        if([BNTaxi isValidWithRepresentation:entry]) {
            
            [array addObject:[BNTaxi fromRepresentation:entry]];
        }
    }
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(taxisRequest:didReturnTaxis:)]) {
        
        [self.delegate taxisRequest:self didReturnTaxis:array];
    }
}

- (void)fail {
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(taxisRequestDidFail:)]) {
        
        [self.delegate taxisRequestDidFail:self];
    }
}

- (void)dealloc {
    
    _delegate = nil;
    [super dealloc];
}

@end
