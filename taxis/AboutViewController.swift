//
//  AboutViewController.swift
//  taxis
//
//  Created by Fabio Dela Antonio on 10/8/15.
//  Copyright © 2015 fabio. All rights reserved.
//

import UIKit

@objc class AboutViewController: UIViewController {

    @IBOutlet weak var infoLabel : UILabel!
    @IBOutlet weak var backgroundImage : UIImageView!
    
    static func aboutViewController() -> AboutViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        return storyboard.instantiateViewControllerWithIdentifier("AboutViewController") as! AboutViewController
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.navigationItem.title = "Perfil"
        
        infoLabel.attributedText = NSAttributedString(BBCode: "[color hex=\"000000\"][font name=\"OpenSans-Bold\" size=\"14\"]Fabio de Albuquerque Dela Antonio[/font]\n\n[font name=\"OpenSans-Semibold\" size=\"14\"][font name=\"FontAwesome\"]\u{f0d5}[/font] fabio914[/font]\n[font name=\"OpenSans\" size=\"14\"][font name=\"FontAwesome\"]\u{f08e}[/font] fabio914.blogspot.com[/font][/color]")
        
        // Parallax
        let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: UIInterpolatingMotionEffectType.TiltAlongVerticalAxis)
        
        vertical.minimumRelativeValue = NSNumber(float: 40)
        vertical.maximumRelativeValue = NSNumber(float: -40)
        
        let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: UIInterpolatingMotionEffectType.TiltAlongHorizontalAxis)
        
        horizontal.minimumRelativeValue = NSNumber(float: 40)
        horizontal.maximumRelativeValue = NSNumber(float: -40)
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontal, vertical]
        
        backgroundImage.addMotionEffect(group)
    }
    
    @IBAction func openPage(sender: AnyObject?) {
        
        if let url = NSURL(string: "http://fabio914.blogspot.com") {
        
            UIApplication.sharedApplication().openURL(url)
        }
    }
}
