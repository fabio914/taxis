//
//  AppDelegate.h
//  taxis
//
//  Created by Fabio Dela Antonio on 10/5/15.
//  Copyright © 2015 fabio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

