//
//  BNQuadNode.h
//  taxis
//
//  Created by Fabio Dela Antonio on 10/6/15.
//  Copyright © 2015 fabio. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BNQuadNode;

@interface BNQuadNode : NSObject

@property (nonatomic, retain, readonly) BNQuadNode * topLeft;
@property (nonatomic, retain, readonly) BNQuadNode * topRight;
@property (nonatomic, retain, readonly) BNQuadNode * bottomLeft;
@property (nonatomic, retain, readonly) BNQuadNode * bottomRight;

@property (nonatomic, assign, readonly) CLLocationCoordinate2D northEast;
@property (nonatomic, assign, readonly) CLLocationCoordinate2D southWest;

- (instancetype)initWithSouthWest:(CLLocationCoordinate2D)southWest northEast:(CLLocationCoordinate2D)northEast;
- (void)addAnnotation:(id<MKAnnotation>)annotation;
- (NSArray *)annotations; // of id<MKAnnotation>
- (NSArray *)annotationsInRectWithSouthWest:(CLLocationCoordinate2D)southWest northEast:(CLLocationCoordinate2D)northEast; // of id<MKAnnotation>

@end
