//
//  BNQuadNode.m
//  taxis
//
//  Created by Fabio Dela Antonio on 10/6/15.
//  Copyright © 2015 fabio. All rights reserved.
//

#import "BNQuadNode.h"

@interface BNQuadNode ()
@property (nonatomic, retain) id<MKAnnotation> annotation;
@end

@implementation BNQuadNode

- (instancetype)initWithSouthWest:(CLLocationCoordinate2D)southWest northEast:(CLLocationCoordinate2D)northEast {
    
    if(self = [super init]) {
        
        if(_southWest.latitude <= _northEast.latitude && _southWest.longitude <= _northEast.longitude) {
        
            _southWest = southWest;
            _northEast = northEast;
        }
        
        else {
            
            DLog(@"Retangulo mal formado");
        }
    }
    
    return self;
}

- (BOOL)isSplit {
    return _topRight != nil && _topLeft != nil && _bottomLeft != nil && _bottomRight != nil;
}

- (BOOL)containsCoordinate:(CLLocationCoordinate2D)coordinate {
    
    return ((_southWest.latitude <= coordinate.latitude && _southWest.longitude <= coordinate.longitude) && (coordinate.latitude <= _northEast.latitude && coordinate.longitude <= _northEast.longitude));
}

- (void)addAnnotation:(id<MKAnnotation>)annotation {
    
    if(_annotation != nil) {
        
        if(![self isSplit]) {
            
            double midLat = (_northEast.latitude + _southWest.latitude)/2.f;
            double midLon = (_northEast.longitude + _southWest.longitude)/2.f;
            
            _topLeft = [[BNQuadNode alloc] initWithSouthWest:CLLocationCoordinate2DMake(midLat, _southWest.longitude) northEast:CLLocationCoordinate2DMake(_northEast.latitude, midLon)];
            _topRight = [[BNQuadNode alloc] initWithSouthWest:CLLocationCoordinate2DMake(midLat, midLon) northEast:_northEast];
            _bottomLeft = [[BNQuadNode alloc] initWithSouthWest:_southWest northEast:CLLocationCoordinate2DMake(midLat, midLon)];
            _bottomRight = [[BNQuadNode alloc] initWithSouthWest:CLLocationCoordinate2DMake(_southWest.latitude, midLon) northEast:CLLocationCoordinate2DMake(midLat, _northEast.longitude)];
            
            [self addAnnotationToSubtree:_annotation];
            [_annotation release], _annotation = nil;
        }
        
        [self addAnnotationToSubtree:annotation];
    }
    
    else {
        
        if([self isSplit]) {
            
            [self addAnnotationToSubtree:annotation];
        }
        
        else {
            
            if([self containsCoordinate:[annotation coordinate]]) {
            
                _annotation = [annotation retain];
            }
            
            else {
                
                DLog(@"Tentou adicionar coordenada que nao faz parte do espaco deste nó");
            }
        }
    }
}

- (void)addAnnotationToSubtree:(id<MKAnnotation>)annotation {
    
    if([_topLeft containsCoordinate:[annotation coordinate]]) {
        
        [_topLeft addAnnotation:annotation];
    }
    
    else if([_topRight containsCoordinate:[annotation coordinate]]) {
        
        [_topRight addAnnotation:annotation];
    }
    
    else if([_bottomLeft containsCoordinate:[annotation coordinate]]) {
        
        [_bottomLeft addAnnotation:annotation];
    }
    
    else if([_bottomRight containsCoordinate:[annotation coordinate]]) {
        
        [_bottomRight addAnnotation:annotation];
    }
    
    else {
        
        DLog(@"Tentou adicionar coordenada que nao faz parte do espaco deste nó");
    }
}

- (NSArray *)annotations {
    
    if(_annotation == nil) {
        
        if([self isSplit]) {
            
            NSMutableArray * annotations = [NSMutableArray array];
            
            [annotations addObjectsFromArray:[_topLeft annotations]];
            [annotations addObjectsFromArray:[_topRight annotations]];
            [annotations addObjectsFromArray:[_bottomLeft annotations]];
            [annotations addObjectsFromArray:[_bottomRight annotations]];
            
            return annotations;
        }
        
        else {
            
            return @[];
        }
    }
    
    else {
        
        return @[_annotation];
    }
}

- (BOOL)interceptsRectWithSouthWest:(CLLocationCoordinate2D)southWest northEast:(CLLocationCoordinate2D)northEast {

    return (_southWest.longitude <= northEast.longitude) && (_northEast.longitude >= southWest.longitude)
    && (_southWest.latitude <= northEast.latitude) && (_northEast.latitude >= southWest.latitude);
}

- (NSArray *)annotationsInRectWithSouthWest:(CLLocationCoordinate2D)southWest northEast:(CLLocationCoordinate2D)northEast {
    
    if(![self interceptsRectWithSouthWest:southWest northEast:northEast]) {
        
        return @[];
    }
    
    if(_annotation) {
        
        CLLocationCoordinate2D coords = [_annotation coordinate];
        
        if((southWest.latitude <= coords.latitude && southWest.longitude <= coords.longitude) && (coords.latitude <= northEast.latitude && coords.longitude <= northEast.longitude)) {
        
            return @[_annotation];
        }
        
        else {
            
            return @[];
        }
    }
    
    else if([self isSplit]) {
        
        NSMutableArray * annotations = [NSMutableArray array];
        
        [annotations addObjectsFromArray:[_topLeft annotationsInRectWithSouthWest:southWest northEast:northEast]];
        [annotations addObjectsFromArray:[_topRight annotationsInRectWithSouthWest:southWest northEast:northEast]];
        [annotations addObjectsFromArray:[_bottomLeft annotationsInRectWithSouthWest:southWest northEast:northEast]];
        [annotations addObjectsFromArray:[_bottomRight annotationsInRectWithSouthWest:southWest northEast:northEast]];
        
        return annotations;
    }
    
    else {
        
        return @[];
    }
}

- (NSDictionary *)asRepresentation {
    
    NSMutableDictionary * dictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"sw":@{@"latitude":@(_southWest.latitude), @"longitude":@(_southWest.longitude)},
                                                                                       @"ne":@{@"latitude":@(_northEast.latitude), @"longitude":@(_northEast.longitude)}}];
    if(_annotation == nil) {
        
        if([self isSplit]) {
            
            dictionary[@"topLeft"] = [_topLeft asRepresentation];
            dictionary[@"topRight"] = [_topRight asRepresentation];
            dictionary[@"bottomLeft"] = [_bottomLeft asRepresentation];
            dictionary[@"bottomRight"] = [_bottomRight asRepresentation];
            
            return dictionary;
        }
        
        else {
            
            return dictionary;
        }
    }
    
    else {
        
        dictionary[@"annotation"] = [_annotation description];
        return dictionary;
    }
}

- (NSString *)description {
    
    return [NSString stringWithFormat:@"%@", [self asRepresentation]];
}

- (void)dealloc {
    
    [_topLeft release], _topLeft = nil;
    [_topRight release], _topRight = nil;
    [_bottomLeft release], _bottomLeft = nil;
    [_bottomRight release], _bottomRight = nil;
    
    [_annotation release], _annotation = nil;
    
    [super dealloc];
}

@end
