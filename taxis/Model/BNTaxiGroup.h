//
//  BNTaxiGroup.h
//  taxis
//
//  Created by Fabio Dela Antonio on 10/6/15.
//  Copyright © 2015 fabio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BNTaxi.h"

@interface BNTaxiGroup : NSObject<MKAnnotation>

@property (nonatomic, assign, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, assign, readonly) NSUInteger count;

+ (instancetype)taxiGroupWithTaxis:(NSArray *)taxis;
- (instancetype)initWithTaxis:(NSArray *)taxis; // of BNTaxi

- (NSString *)title;
- (NSString *)subtitle;

@end
