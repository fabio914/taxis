//
//  BNTaxiGroup.m
//  taxis
//
//  Created by Fabio Dela Antonio on 10/6/15.
//  Copyright © 2015 fabio. All rights reserved.
//

#import "BNTaxiGroup.h"

@interface BNTaxiGroup ()
@property (nonatomic, retain) NSString * groupTitle;
@property (nonatomic, retain) NSString * groupSubtitle;
@end

@implementation BNTaxiGroup

+ (instancetype)taxiGroupWithTaxis:(NSArray *)taxis {
    return [[[self alloc] initWithTaxis:taxis] autorelease];
}

- (instancetype)initWithTaxis:(NSArray *)taxis {
    
    if(self = [super init]) {
        
        NSMutableString * title = [NSMutableString string];
        NSUInteger count = 0, countOfAvailable = 0;
        double latitude = 0.f, longitude = 0.f;
        
        for(id entry in taxis) {
            
            if([entry isKindOfClass:[BNTaxi class]]) {
                
                count++;
                countOfAvailable += [(BNTaxi *)entry isAvailable];
                
                latitude += [(BNTaxi *)entry coordinate].latitude;
                longitude += [(BNTaxi *)entry coordinate].longitude;
                [title appendFormat:@"%u ", (unsigned)[(BNTaxi *)entry driverId]];
            }
        }
        
        if(count > 0) {
        
            _coordinate = CLLocationCoordinate2DMake(latitude/(double)count, longitude/(double)count);
        }
        
        _count = count;
        _groupTitle = [[NSString alloc] initWithString:title];
        _groupSubtitle = [[NSString stringWithFormat:@"%u disponíveis de %u", (unsigned)countOfAvailable, (unsigned)count] retain];
    }
    
    return self;
}

- (NSString *)title {
    return _groupTitle;
}

- (NSString *)subtitle {
    return _groupSubtitle;
}

- (void)dealloc {
    
    [_groupTitle release], _groupTitle = nil;
    [_groupSubtitle release], _groupSubtitle = nil;
    [super dealloc];
}

@end
