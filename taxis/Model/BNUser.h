//
//  BNUser.h
//  taxis
//
//  Created by Fabio Dela Antonio on 10/5/15.
//  Copyright © 2015 fabio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BNUser : NSObject <MKAnnotation>

@property (nonatomic, assign, readonly) CLLocationCoordinate2D coordinate;

+ (instancetype)userWithCoordinate:(CLLocationCoordinate2D)coordinate;
- (NSString *)title;

@end
