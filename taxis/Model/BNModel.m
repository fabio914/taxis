//
//  BNModel.m
//  taxis
//
//  Created by Fabio Dela Antonio on 10/5/15.
//  Copyright © 2015 fabio. All rights reserved.
//

#import "BNModel.h"

@implementation BNModel

+ (BOOL)isValidWithRepresentation:(NSDictionary *)representation {
    
    if(![representation isKindOfClass:[NSDictionary class]])
        return NO;
    
    return YES;
}

+ (instancetype)fromRepresentation:(NSDictionary *)representation {
    return [[[self alloc] initWithRepresentation:representation] autorelease];
}

- (instancetype)initWithRepresentation:(NSDictionary *)representation {
    
    if(self = [super init]) {
        
    }
    
    return self;
}


- (NSDictionary *)asRepresentation {
    return @{};
}

@end
