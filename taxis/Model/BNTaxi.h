//
//  BNTaxi.h
//  taxis
//
//  Created by Fabio Dela Antonio on 10/5/15.
//  Copyright © 2015 fabio. All rights reserved.
//

#import "BNModel.h"
#import "BNUser.h"

@interface BNTaxi : BNModel <MKAnnotation>

@property (nonatomic, assign, readonly) CLLocationCoordinate2D coordinate;

@property (nonatomic, assign, readonly) NSUInteger driverId;
@property (nonatomic, assign, readonly, getter=isAvailable) BOOL available;
@property (nonatomic, assign, readonly) double rating;

- (NSNumber *)distanceToUser:(BNUser *)user; /* km */

- (NSString *)title;
- (NSString *)subtitle;

@end
