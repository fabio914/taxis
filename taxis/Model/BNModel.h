//
//  BNModel.h
//  taxis
//
//  Created by Fabio Dela Antonio on 10/5/15.
//  Copyright © 2015 fabio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BNModel : NSObject

+ (BOOL)isValidWithRepresentation:(NSDictionary *)representation;
+ (instancetype)fromRepresentation:(NSDictionary *)representation;

- (instancetype)initWithRepresentation:(NSDictionary *)representation;
- (NSDictionary *)asRepresentation;

@end
