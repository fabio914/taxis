//
//  BNUser.m
//  taxis
//
//  Created by Fabio Dela Antonio on 10/5/15.
//  Copyright © 2015 fabio. All rights reserved.
//

#import "BNUser.h"

@implementation BNUser

+ (instancetype)userWithCoordinate:(CLLocationCoordinate2D)coordinate {
    return [[[self alloc] initWithCoordinate:coordinate] autorelease];
}

- (instancetype)initWithCoordinate:(CLLocationCoordinate2D)coordinate {
    
    if(self = [super init]) {
        
        _coordinate = coordinate;
    }
    
    return self;
}

- (NSString *)title {
    
    return @"Sua localização";
}

- (void)dealloc {
    
    [super dealloc];
}

@end
