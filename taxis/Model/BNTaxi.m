//
//  BNTaxi.m
//  taxis
//
//  Created by Fabio Dela Antonio on 10/5/15.
//  Copyright © 2015 fabio. All rights reserved.
//

#import "BNTaxi.h"

@implementation BNTaxi

+ (BOOL)isValidWithRepresentation:(NSDictionary *)representation {
    
    if(![super isValidWithRepresentation:representation])
        return NO;
    
    if(![representation[@"latitude"] isKindOfClass:[NSNumber class]])
        return NO;
    
    if(![representation[@"longitude"] isKindOfClass:[NSNumber class]])
        return NO;
    
    double latitude = [(NSNumber *)representation[@"latitude"] doubleValue];
    double longitude = [(NSNumber *)representation[@"longitude"] doubleValue];
    
    if(fabs(latitude) > 90.f)
        return NO;
    
    if(fabs(longitude) > 180.f)
        return NO;
    
    if(![representation[@"driverId"] isKindOfClass:[NSNumber class]])
        return NO;
    
    if([(NSNumber *)representation[@"driverId"] integerValue] < 0)
        return NO;
    
    if(![representation[@"driverAvailable"] isKindOfClass:[NSNumber class]])
        return NO;
    
    return YES;
}

- (instancetype)initWithRepresentation:(NSDictionary *)representation {
    
    if(self = [super initWithRepresentation:representation]) {
        
        _coordinate = CLLocationCoordinate2DMake([(NSNumber *)representation[@"latitude"] doubleValue], [(NSNumber *)representation[@"longitude"] doubleValue]);
        _driverId = [(NSNumber *)representation[@"driverId"] unsignedIntegerValue];
        _available = [(NSNumber *)representation[@"driverAvailable"] boolValue];
        
        /* Usando rating aleatorio, ja que nao tenho este dado... */
        _rating = rand()/(double)RAND_MAX;
    }
    
    return self;
}

- (NSNumber *)distanceToUser:(BNUser *)user {
    
    if(user == nil) {
        
        return nil;
    }
    
    /* Utilizando distancia linear mas o ideal seria distancia pelas ruas... */
    CLLocation * userLocation = [[CLLocation alloc] initWithLatitude:user.coordinate.latitude longitude:user.coordinate.longitude];
    CLLocation * taxiLocation = [[CLLocation alloc] initWithLatitude:self.coordinate.latitude longitude:self.coordinate.longitude];
    
    NSNumber * result = @([taxiLocation distanceFromLocation:userLocation]/1000.f);
    
    [userLocation release], userLocation = nil;
    [taxiLocation release], taxiLocation = nil;
    
    return result;
}

- (NSString *)title {
    return [NSString stringWithFormat:@"%u", (unsigned)_driverId];
}

- (NSString *)subtitle {
    return _available ? @"Disponível":@"Não disponível";
}

- (NSString *)description {
    return [NSString stringWithFormat:@"BNTaxi ID: %u Latitude: %lf Longitude: %lf", (unsigned)_driverId, _coordinate.latitude, _coordinate.longitude];
}

- (void)dealloc {
    
    [super dealloc];
}

@end
