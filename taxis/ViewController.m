//
//  ViewController.m
//  taxis
//
//  Created by Fabio Dela Antonio on 10/5/15.
//  Copyright © 2015 fabio. All rights reserved.
//

#import "ViewController.h"
#import "taxis-Swift.h"

#import "BNTableView.h"
#import "BNTaxiCell.h"
#import "BNSeparatorCell.h"

#import "BNUser.h"
#import "BNTaxiGroup.h"

#import "BNNetworkManager.h"
#import "BNTaxisRequest.h"

#import "BNQuadNode.h"

#import "BNGroupAnnotation.h"
#import "BNTaxiAnnotation.h"

#import "MKMapView+ZoomLevel.h"
#import "NSAttributedString+bbCode.h"

#define kTimerInterval 5.f /* s */

@interface ViewController () <MKMapViewDelegate, CLLocationManagerDelegate, BNTaxisRequestDelegate, BNTaxiAnnotationDelegate, BNTaxiCellDelegate>
@property (retain, nonatomic) IBOutlet MKMapView * mapView;
@property (retain, nonatomic) IBOutlet UILabel * locationLabel;
@property (retain, nonatomic) IBOutlet UILabel * timerLabel;

@property (retain, nonatomic) CLLocationManager * locationManager;

@property (retain, nonatomic) BNUser * user;
@property (nonatomic, assign) NSTimeInterval timer;

@property (nonatomic, assign) BOOL extraInfoShown;
@property (retain, nonatomic) IBOutlet UIView * extraInfoView;
@property (retain, nonatomic) IBOutlet UIView * tabView;
@property (retain, nonatomic) IBOutlet UILabel * tabViewLabel;
@property (retain, nonatomic) IBOutlet BNTableView * tableView;

@end

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [_locationLabel setAttributedText:BBF("[font name=\"FontAwesome\" size=\"20\"]\uf124[/font]")];
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    
    _mapView.delegate = self;
}

- (void)viewDidLayoutSubviews {
    
    [super viewDidLayoutSubviews];
    [_extraInfoView setFrame:CGRectMake(_extraInfoView.frame.origin.x, _extraInfoView.frame.origin.y, _extraInfoView.frame.size.width, self.view.frame.size.height - 64.f)];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [_locationManager requestAlwaysAuthorization];
    [_locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    [_locationManager setPausesLocationUpdatesAutomatically:YES];
    [_locationManager setDistanceFilter:10.f];
    [_locationManager startUpdatingLocation];
    
    if(![self extraInfoShown]) {
    
        [self reload];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [_locationManager stopUpdatingLocation];
    [BNNetworkManager cancelRequestsWithTarget:self];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

- (void)reload {
    
    /* Limpa timer */
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    _timer = kTimerInterval + 1.f;
    [self update];
    
    CLLocationCoordinate2D southWest = [self.mapView convertPoint:CGPointMake(0, self.mapView.frame.size.height) toCoordinateFromView:self.mapView];
    CLLocationCoordinate2D northEast = [self.mapView convertPoint:CGPointMake(self.mapView.frame.size.width, 0) toCoordinateFromView:self.mapView];
    
    /* Compensa rotacoes do mapa */
    CLLocationCoordinate2D max = CLLocationCoordinate2DMake(MAX(southWest.latitude, northEast.latitude), MAX(southWest.longitude, northEast.longitude));
    CLLocationCoordinate2D min = CLLocationCoordinate2DMake(MIN(southWest.latitude, northEast.latitude), MIN(southWest.longitude, northEast.longitude));
    
    /* Adiciona novo request */
    [BNNetworkManager cancelRequestsWithTarget:self];
    [BNNetworkManager addRequest:[BNTaxisRequest taxisRequestWithSouthWest:min northEast:max delegate:self]];
}

#pragma mark - Taxis Request Delegate

- (void)taxisRequest:(BNTaxisRequest *)request didReturnTaxis:(NSArray *)taxis {
    
    if([taxis count]) {
        
        [self.mapView removeAnnotations:self.mapView.annotations];
        
        if(self.user) {
            
            [self.mapView addAnnotation:self.user];
        }
        
        /* Adiciona taxis na tabela */
        NSArray * sortedTaxis = taxis;
        
        /* Obs.: Trecho abaixo (ordenacao e lista) esta recalculando a mesma distancia multiplas vezes, poderia armazenar para otimizar */
        
        if(self.user) {
            
            sortedTaxis = [taxis sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
               
                BNTaxi * taxi1 = (BNTaxi *)obj1, * taxi2 = (BNTaxi *)obj2;
                return ([[taxi1 distanceToUser:self.user] doubleValue] < [[taxi2 distanceToUser:self.user] doubleValue]) ? NSOrderedAscending:NSOrderedDescending;
            }];
        }
    
        NSMutableArray * cells = [NSMutableArray array];
        
        for(BNTaxi * taxi in sortedTaxis) {
            
            [cells addObject:[BNTaxiCell taxiCellWithTaxi:taxi distance:[taxi distanceToUser:self.user] delegate:self]];
            
            if(taxi != [sortedTaxis lastObject]) {
                
                [cells addObject:[BNSeparatorCell separatorCell]];
            }
        }
        
        [self.tableView setCells:cells];
        
        /* Adiciona taxis na quad tree */
        BNQuadNode * tree = [[BNQuadNode alloc] initWithSouthWest:request.southWest northEast:request.northEast];
        
        for(BNTaxi * taxi in taxis) {
            
            [tree addAnnotation:taxi];
        }
        
        /* Itera grid agrupando taxis */
        NSUInteger nSquares = (NSUInteger)ceil([self.mapView zoomLevel]/2.f);
        
        /* Grid mais fino com mais zoom (menos clustering) */
        double squareWidth = fabs(request.northEast.longitude - request.southWest.longitude)/nSquares;
        double squareHeight = fabs(request.northEast.latitude - request.southWest.latitude)/nSquares;
        
        for(unsigned i = 0; i < nSquares; i++) {
            
            for(unsigned j = 0; j < nSquares; j++) {
                
                NSArray * annotations = [tree annotationsInRectWithSouthWest:CLLocationCoordinate2DMake(request.southWest.latitude + (squareHeight * (double)i), request.southWest.longitude + (squareWidth * (double)j))
                                                                   northEast:CLLocationCoordinate2DMake(request.southWest.latitude + (squareHeight * (double)(i + 1)), request.southWest.longitude + (squareWidth * (double)(j + 1)))];
                
                NSUInteger count = [annotations count];
                
                if(count == 1) {
                    
                    [self.mapView addAnnotation:[annotations firstObject]];
                }
                
                else if(count > 1) {
                    
                    [self.mapView addAnnotation:[BNTaxiGroup taxiGroupWithTaxis:annotations]];
                }
            }
        }
        
        [tree release], tree = nil;
    }
}

- (void)taxisRequestDidFail:(BNTaxisRequest *)request {
    
    DLog(@"Request falhou.");
}

#pragma mark - Map Delegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    
    NSString * identifier = NSStringFromClass([annotation class]);
    MKAnnotationView * pin = [mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    
    if(pin == nil) {
        
        Class pinClass = [MKAnnotationView class];
        
        if([annotation isKindOfClass:[BNTaxiGroup class]]) {
            
            pinClass = [BNGroupAnnotation class];
        }
        
        else if([annotation isKindOfClass:[BNTaxi class]]) {
            
            pinClass = [BNTaxiAnnotation class];
        }
        
        pin = [[[pinClass alloc] initWithAnnotation:annotation reuseIdentifier:identifier] autorelease];
    }
    
    if([annotation isKindOfClass:[BNTaxi class]]) {
        
        [(BNTaxiAnnotation *)pin setDelegate:self];
        pin.annotation = annotation;
    }
        
    else if([annotation isKindOfClass:[BNTaxiGroup class]]) {
        
        pin.annotation = annotation;
    }
    
    else if([annotation isKindOfClass:[BNUser class]]) {
        
        pin.canShowCallout = YES;
        pin.image = [UIImage imageNamed:@"Pin-passenger-down"];
        pin.centerOffset = CGPointMake(0, -pin.image.size.height/2.f);
    }
    
    return pin;
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    
    [self reload];
}

#pragma mark - Location Manager

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    if(status != kCLAuthorizationStatusAuthorizedAlways && status != kCLAuthorizationStatusNotDetermined) {
        
        ERROR_ALERT(@"Deve autorizar o uso da localização.");
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    CLLocation * newLocation = [locations lastObject];
    
    BOOL first = (self.user == nil);
    
    if(self.user) {
        
        [self.mapView removeAnnotation:self.user];
    }
    
    self.user = [BNUser userWithCoordinate:[newLocation coordinate]];
    
    [self.mapView addAnnotation:self.user];
    
    if(first) {
    
        [self focus];
    }
}

#pragma mark - Timer

- (void)update {
    
    if(_timer <= 1.f) {
        
        [self reload];
        return;
    }
    
    _timer -= 1.f;
    [self.timerLabel setAttributedText:BBF("[font name=\"OpenSans-Semibold\" size=\"20\"]%.0f s[/font]  [font name=\"FontAwesome\" size=\"20\"]\uf021[/font]", _timer)];
    
    /* Poderia utilizar um NSTimer ao invés disso */
    [self performSelector:@selector(update) withObject:nil afterDelay:1.f];
}

- (void)pauseTimer {
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

- (void)restartTimer {
    
    [self update];
}

#pragma mark - Extra Info

- (void)hiddenState {
    
    self.tabViewLabel.text = @"+";
    [self.tabView setFrame:CGRectMake(self.tabView.frame.origin.x, self.view.frame.size.height - self.tabView.frame.size.height + 10.f, self.tabView.frame.size.width, self.tabView.frame.size.height)];
    [self.tabView setBackgroundColor:[UIColor colorWithWhite:0.f alpha:0.7f]];
    [self.extraInfoView setFrame:CGRectMake(0, self.view.frame.size.height, self.extraInfoView.frame.size.width, self.extraInfoView.frame.size.height)];
}

- (void)shownState {
    
    self.tabViewLabel.text = @"-";
    [self.tabView setFrame:CGRectMake(self.tabView.frame.origin.x, 64.f, self.tabView.frame.size.width, self.tabView.frame.size.height)];
    [self.tabView setBackgroundColor:[UIColor clearColor]];
    [self.extraInfoView setFrame:CGRectMake(0, 64.f, self.extraInfoView.frame.size.width, self.extraInfoView.frame.size.height)];
}

- (void)showExtraInfo {
    
    if(_extraInfoShown)
        return;

    [self pauseTimer];
    [self hiddenState];
    
    [UIView animateWithDuration:0.5 animations:^{
        
        [self shownState];
        
    } completion:^(BOOL finished) {
        
        if(finished) {
            
            _extraInfoShown = YES;
        }
    }];
}

- (void)hideExtraInfo {
    
    if(!_extraInfoShown)
        return;
    
    [self shownState];
    
    [UIView animateWithDuration:0.5 animations:^{
        
        [self hiddenState];
        
    } completion:^(BOOL finished) {
        
        if(finished) {
            
            _extraInfoShown = NO;
            [self restartTimer];
        }
    }];
}

#pragma mark - Taxi Details

- (void)taxiAnnotationView:(BNTaxiAnnotation *)view openDetailsForTaxi:(BNTaxi *)taxi {
    
    [self openDetailsForTaxi:taxi];
}

- (void)taxiCell:(BNTaxiCell *)cell openDetailsForTaxi:(BNTaxi *)taxi {
    
    [self openDetailsForTaxi:taxi];
}

- (void)openDetailsForTaxi:(BNTaxi *)taxi {

    [self.navigationController pushViewController:[TaxiDetailViewController taxiDetailViewController:taxi] animated:YES];
}

#pragma mark - Actions

- (void)focus {
    
    if(self.user) {
    
        [self.mapView setCenterCoordinate:[self.user coordinate] zoomLevel:17.f animated:YES];
    }
}

- (IBAction)focusAction:(id)sender {
    [self focus];
}

- (IBAction)reloadAction:(id)sender {
    [self reload];
}

- (IBAction)showOrHideExtraAction:(id)sender {
    
    if(_extraInfoShown) {
        
        [self hideExtraInfo];
    }
    
    else {
        
        [self showExtraInfo];
    }
}

- (IBAction)profileButton:(id)sender {

    [self.navigationController pushViewController:[AboutViewController aboutViewController] animated:YES];
}

- (void)dealloc {
    
    [BNNetworkManager cancelRequestsWithTarget:self];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    [_locationManager release], _locationManager = nil;
    [_mapView release], _mapView = nil;
    [_user release], _user = nil;
    
    [_locationLabel release];
    [_timerLabel release];
    [_extraInfoView release];
    [_tabView release];
    [_tabViewLabel release];
    [_tableView release];
    [super dealloc];
}

@end
