//
//  BNBBLabel.m
//  taxis
//
//  Created by Fabio Dela Antonio on 10/5/15.
//  Copyright © 2015 fabio. All rights reserved.
//

#import "BNBBLabel.h"
#import "NSAttributedString+bbCode.h"

@implementation BNBBLabel

- (void)setBbCode:(NSString *)bbCode {
    
    if(bbCode != _bbCode && ![bbCode isEqualToString:_bbCode]) {
    
        [_bbCode release];
        _bbCode = [bbCode retain];
        [self setAttributedText:[NSAttributedString attributedStringWithBBCode:bbCode]];
    }
}

- (void)dealloc {
    
    [_bbCode release], _bbCode = nil;
    [super dealloc];
}

@end
