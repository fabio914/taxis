//
//  BNBBLabel.h
//  taxis
//
//  Created by Fabio Dela Antonio on 10/5/15.
//  Copyright © 2015 fabio. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface BNBBLabel : UILabel

@property (nonatomic, retain) IBInspectable NSString * bbCode;

@end

