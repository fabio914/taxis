//
//  BNTaxiAnnotation.m
//  taxis
//
//  Created by Fabio Dela Antonio on 10/8/15.
//  Copyright © 2015 fabio. All rights reserved.
//

#import "BNTaxiAnnotation.h"

@interface BNTaxiAnnotation ()
@property (nonatomic, retain) BNTaxi * taxi;
@end

@implementation BNTaxiAnnotation

- (void)setAnnotation:(id<MKAnnotation>)annotation {
    
    [super setAnnotation:annotation];
    
    [_taxi release], _taxi = nil;
    
    if([annotation isKindOfClass:[BNTaxi class]]) {
        
        self.canShowCallout = YES;
        self.image = [UIImage imageNamed:[(BNTaxi *)annotation isAvailable] ? @"Pin-taxi":@"Pin-taxi-nop"];
        self.centerOffset = CGPointMake(0, -self.image.size.height/2.f);
        
        if(_delegate && [_delegate respondsToSelector:@selector(taxiAnnotationView:openDetailsForTaxi:)]) {
        
            UIButton * button = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            [button addTarget:self action:@selector(openDetails) forControlEvents:UIControlEventTouchUpInside];
            
            self.rightCalloutAccessoryView = button;
            _taxi = [(BNTaxi *)annotation retain];
        }
        
        else {
            
            self.rightCalloutAccessoryView = nil;
        }
    }
    
    else {
        
        self.canShowCallout = NO;
        self.image = nil;
        self.rightCalloutAccessoryView = nil;
    }
}

- (void)openDetails {
    
    if(_delegate && [_delegate respondsToSelector:@selector(taxiAnnotationView:openDetailsForTaxi:)]) {
        
        [_delegate taxiAnnotationView:self openDetailsForTaxi:_taxi];
    }
}

- (void)dealloc {
    
    [_taxi release], _taxi = nil;
    [super dealloc];
}

@end
