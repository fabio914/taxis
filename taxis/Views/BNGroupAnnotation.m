//
//  BNGroupAnnotation.m
//  taxis
//
//  Created by Fabio Dela Antonio on 10/7/15.
//  Copyright © 2015 fabio. All rights reserved.
//

#import "BNGroupAnnotation.h"
#import "BNTaxiGroup.h"

@interface BNGroupAnnotation ()
@property (nonatomic, retain) UILabel * label;
@end

@implementation BNGroupAnnotation

- (instancetype)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    
    if(self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier]) {
        
        CGRect frame = CGRectMake(0, 0, 40.f, 40.f);
        
        UIView * background = [[UIView alloc] initWithFrame:frame];
        [background.layer setCornerRadius:20.f];
        [background setClipsToBounds:YES];
        [background setBackgroundColor:[UIColor blackColor]];
        
        UILabel * label = [[UILabel alloc] initWithFrame:frame];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setTextColor:kYellowColour];
        
        if([[super annotation] isKindOfClass:[BNTaxiGroup class]]) {
            
            [label setText:[NSString stringWithFormat:@"%u", (unsigned)[(BNTaxiGroup *)[super annotation] count]]];
        }
        
        [background addSubview:label];
        _label = label;
        
        [self addSubview:background];
        [background release], background = nil;
        [self setFrame:frame];
    }
    
    return self;
}

- (void)adjust {
    
    if([[super annotation] isKindOfClass:[BNTaxiGroup class]]) {
        
        [_label setText:[NSString stringWithFormat:@"%u", (unsigned)[(BNTaxiGroup *)[super annotation] count]]];
    }
    
    else {
        
        [_label setText:@""];
    }
}

- (void)setAnnotation:(id<MKAnnotation>)annotation {
    
    [super setAnnotation:annotation];
    [self setCanShowCallout:YES];
    [self adjust];
}

- (void)prepareForReuse {
    
    [_label setText:@""];
}

- (void)dealloc {
    
    [_label release], _label = nil;
    [super dealloc];
}

@end
