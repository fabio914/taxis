//
//  BNTaxiCell.m
//  taxis
//
//  Created by Fabio Dela Antonio on 10/7/15.
//  Copyright © 2015 fabio. All rights reserved.
//

#import "BNTaxiCell.h"
#import "NSAttributedString+bbCode.h"

@interface BNTaxiCellView ()
@property (retain, nonatomic) IBOutlet UILabel * topLabel;
@property (retain, nonatomic) IBOutlet UILabel * bottomLabel;
@property (retain, nonatomic) IBOutlet UILabel * rightLabel;
@end

@interface BNTaxiCell ()
@property (nonatomic, retain) NSAttributedString * taxiInfo;
@property (nonatomic, retain) NSAttributedString * taxiStars;
@property (nonatomic, retain) NSAttributedString * taxiDistance;
@property (nonatomic, retain) BNTaxi * taxi;
@end

@implementation BNTaxiCell

+ (NSString *)starsForRating:(double)rating {
    
    NSUInteger stars = (NSUInteger)ceil(rating * 5.f);
    NSMutableString * string = [NSMutableString stringWithFormat:@"[color hex=\"D2A000\"][font name=\"FontAwesome\" size=\"14\"]"];
    NSUInteger i = 0;
    
    for(; i < stars; i++) {
        
        [string appendString:@"\uf005"];
    }
    
    for(; i < 5; i++) {
        
        [string appendString:@"\uf006"];
    }
    
    [string appendString:@"[/font][/color]"];
    
    return string;
}

+ (instancetype)taxiCellWithTaxi:(BNTaxi *)taxi distance:(NSNumber *)distance {
    
    BNTaxiCell * cell = [[self alloc] init];
    
    cell.taxi = taxi;
    cell.taxiInfo = BBF("[color hex=\"FFFFFF\"][font name=\"OpenSans-Semibold\" size=\"16\"]%@[/font] [font name=\"OpenSans\" size=\"14\"]%@[/font][/color]", [taxi title], [taxi subtitle]);
    cell.taxiStars = BBF("%@", [self starsForRating:[taxi rating]]);
    
    if(distance) {
        
        cell.taxiDistance = BBF("[color hex=\"FFFFFF\"][font name=\"OpenSans\" size=\"14\"]%.1lf km[/font][/color]", [distance doubleValue]);
    }
    
    return [cell autorelease];
}

+ (instancetype)taxiCellWithTaxi:(BNTaxi *)taxi distance:(NSNumber *)distance delegate:(id<BNTaxiCellDelegate>)delegate {
    
    BNTaxiCell * cell = [self taxiCellWithTaxi:taxi distance:distance];
    [cell setDelegate:delegate];
    return cell;
}

- (void)tableView:(BNTableView *)tableView cell:(BNTableViewCell *)cell forIndexPath:(NSIndexPath *)indexPath {
    
    BNTaxiCellView * cellView = (BNTaxiCellView *)cell;
    
    [cellView.topLabel setAttributedText:_taxiInfo];
    [cellView.bottomLabel setAttributedText:_taxiStars];
    
    if(_taxiDistance) {
        
        [cellView.rightLabel setHidden:NO];
        [cellView.rightLabel setAttributedText:_taxiDistance];
    }
    
    else {
        
        [cellView.rightLabel setHidden:YES];
    }
}

- (CGFloat)cellHeight {
    return 48.f;
}

- (void)defaultAction {
    
    if(_delegate && [_delegate respondsToSelector:@selector(taxiCell:openDetailsForTaxi:)]) {
        
        [_delegate taxiCell:self openDetailsForTaxi:_taxi];
    }
}

- (void)dealloc {
    
    [_taxi release], _taxi = nil;
    [_taxiInfo release], _taxiInfo = nil;
    [_taxiStars release], _taxiStars = nil;
    [_taxiDistance release], _taxiDistance = nil;
    [super dealloc];
}

@end

@implementation BNTaxiCellView

- (IBAction)clickAction:(id)sender {

    [self.controller defaultAction];
}

- (void)dealloc {
    
    [_topLabel release];
    [_bottomLabel release];
    [_rightLabel release];
    [super dealloc];
}
@end
