//
//  BNTaxiCell.h
//  taxis
//
//  Created by Fabio Dela Antonio on 10/7/15.
//  Copyright © 2015 fabio. All rights reserved.
//

#import "BNTableView.h"
#import "BNTaxi.h"

@class BNTaxiCell;

@protocol BNTaxiCellDelegate <NSObject>

@optional
- (void)taxiCell:(BNTaxiCell *)cell openDetailsForTaxi:(BNTaxi *)taxi;

@end

@interface BNTaxiCell : BNTableCellController

@property (nonatomic, assign) id<BNTaxiCellDelegate> delegate;

+ (instancetype)taxiCellWithTaxi:(BNTaxi *)taxi distance:(NSNumber *)distance;
+ (instancetype)taxiCellWithTaxi:(BNTaxi *)taxi distance:(NSNumber *)distance delegate:(id<BNTaxiCellDelegate>)delegate;

@end

@interface BNTaxiCellView : BNTableViewCell

@end
