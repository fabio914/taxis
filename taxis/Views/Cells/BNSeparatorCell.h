//
//  BNSeparatorCell.h
//  taxis
//
//  Created by Fabio Dela Antonio on 10/7/15.
//  Copyright © 2015 fabio. All rights reserved.
//

#import "BNTableView.h"

@interface BNSeparatorCell : BNTableCellController

+ (instancetype)separatorCell;

@end

@interface BNSeparatorCellView : BNTableViewCell

@end
