//
//  BNSeparatorCell.m
//  taxis
//
//  Created by Fabio Dela Antonio on 10/7/15.
//  Copyright © 2015 fabio. All rights reserved.
//

#import "BNSeparatorCell.h"

@implementation BNSeparatorCell

+ (instancetype)separatorCell {
    return [[[self alloc] init] autorelease];
}

- (void)tableView:(BNTableView *)tableView cell:(BNTableViewCell *)cell forIndexPath:(NSIndexPath *)indexPath {
    
}

- (CGFloat)cellHeight {
    return 1.f;
}

@end

@implementation BNSeparatorCellView

@end