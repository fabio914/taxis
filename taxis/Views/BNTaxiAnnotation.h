//
//  BNTaxiAnnotation.h
//  taxis
//
//  Created by Fabio Dela Antonio on 10/8/15.
//  Copyright © 2015 fabio. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "BNTaxi.h"

@class BNTaxiAnnotation;

@protocol BNTaxiAnnotationDelegate <NSObject>

@optional
- (void)taxiAnnotationView:(BNTaxiAnnotation *)view openDetailsForTaxi:(BNTaxi *)taxi;

@end

@interface BNTaxiAnnotation : MKAnnotationView

@property (nonatomic, assign) id<BNTaxiAnnotationDelegate> delegate;

@end
