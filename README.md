# Taxis
Aplicativo teste que consome serviço da 99Taxis, mostrando taxis em um mapa.

### Funcionalidades
* Mapa com os taxis com pins customizados e com anotação mostrando o ID;
* Atualização dos taxis mostrados a cada 5 segundos, ou quando a região do mapa é alterada, ou quando o botão de atualizar é pressionado; 
* Exibe pin com imagem na localização do usuário;
* Botão com ação para focar o mapa na posição do usuário; 
* Agrupamento dos taxis próximos (no mesmo grid, com tamanho do grid inversamente proporcional ao nível de zoom do mapa), utilizando Quad Tree para indexar os taxis por localização e tornar mais eficiente a consulta dos taxis que ocupam determinado retângulo;
* Tabela com informações dos taxis carregados no último request (colocando informação aleatória de avaliação do taxi), ordenados por distância linear ao usuário;
* Tela de perfil com informações do desenvolvedor (feita em Swift, com efeito de parallax e com botão com ação de carregar página externa);
* Tela de detalhes do taxi (feita em Swift) que pode ser acessada a partir de um pin de taxi no mapa ou a partir da tabela de taxis, e que faz reverse geocoding para carregar e exibir o endereço atual do taxi;

### Detalhes Técnicos
Projeto escrito quase totalmente em Objective-C sem contagem automática de referência, e com duas classes em Swift (AboutViewController e TaxiDetailViewController). Geração de BitCode está desabilitada. Storyboard e XIBs não utilizam auto-layout. 

### Swift
Swift foi utilizado para escrever os View Controllers de Perfil e Detalhe de Taxi por estes serem mais simples e pouco interagirem com o restante da aplicação, assim podendo ser facilmente escritos nesta linguagem.

### Componentes
Este aplicativo utiliza de componentes que desenvolvi para projetos pessoais meus. Entre eles estão componentes para requests de rede (BNNetworkManager, ...), para geração de strings com atributos a partir de uma linguagem de markup baseada em BBCode (NSAttributedString+bbCode), e para abstração de tabelas (BNTableView).

### Open-Source
* [FontAwesome](https://fortawesome.github.io/Font-Awesome/)
* [OpenSans](https://www.google.com/fonts/specimen/Open+Sans)
* Google Open Source Framework

### Observações
Aplicativo utiliza de alguns assets de ícones e imagens extraidos do app 99Taxis.

### Desenvolvedor
[Fabio de Albuquerque Dela Antonio](http://fabio914.blogspot.com)
